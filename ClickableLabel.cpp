#include "ClickableLabel.h"

/// \fn ClickableLabel::ClickableLabel(QWidget *parent)
/// \~English
/// \brief Constructor
/// \~Spanish
/// \brief Constructor
ClickableLabel::ClickableLabel(QWidget *parent) : QLabel(parent) {
    this->x = 0;
    this->y = 0;
}

/// \fn void ClickableLabel::mouseMoveEvent(QMouseEvent *ev)
/// \~English
/// \brief Funtion to add mouse event to the labelWidget
/// \~Spanish
/// \brief Funcion para anadir evento del raton (mouse) al labelWidget
void ClickableLabel::mouseMoveEvent(QMouseEvent *ev){
    emit Mouse_Pos();
    this->x = ev->x();
    this->y = ev->y();
}

/// \fn void ClickableLabel::mousePressEvent(QMouseEvent *ev)
/// \~English
/// \brief Funtion to add mouse event to the labelWidget
/// \~Spanish
/// \brief Funcion para anadir evento del raton (mouse) al labelWidget
void ClickableLabel::mousePressEvent(QMouseEvent *ev){
    x = ev->x();
    y = ev->y();
    emit Mouse_Pressed();
}

/// \fn void ClickableLabel::leaveEvent(QEvent *)
/// \~English
/// \brief Funtion to add mouse event to the labelWidget
/// \~Spanish
/// \brief Funcion para anadir evento del raton (mouse) al labelWidget
void ClickableLabel::leaveEvent(QEvent *){
    emit Mouse_Left();
}
